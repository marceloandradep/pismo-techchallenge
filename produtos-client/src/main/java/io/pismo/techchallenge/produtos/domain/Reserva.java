package io.pismo.techchallenge.produtos.domain;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.pismo.techchallenge.produtos.constants.StatusReserva;

public class Reserva {
	
	private String produtoId;
	private Integer pedidoId;
	
	private Integer quantidade;
	private StatusReserva status;
	
	public Reserva() {
		this(null, null, null);
	}
	
	public Reserva(String produtoId, Integer pedidoId, Integer quantidade) {
		this.produtoId = produtoId;
		this.pedidoId = pedidoId;
		this.quantidade = quantidade;
		this.status = StatusReserva.SOLICITADA;
	}

	public String getProdutoId() {
		return produtoId;
	}

	public void setProdutoId(String produtoId) {
		this.produtoId = produtoId;
	}
	
	public Integer getPedidoId() {
		return pedidoId;
	}

	public void setPedidoId(Integer pedidoId) {
		this.pedidoId = pedidoId;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public StatusReserva getStatus() {
		return status;
	}

	public void setStatus(StatusReserva status) {
		this.status = status;
	}

	public String toJson() throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(this);
	}
	
	public static Reserva fromJson(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, Reserva.class);
	}

	public Reserva confirma() {
		status = StatusReserva.CONFIRMADA;
		return this;
	}

	public Reserva cancela() {
		status = StatusReserva.CANCELADA;
		return this;
	}

	@JsonIgnore
	public boolean isConfirmada() {
		return StatusReserva.CONFIRMADA.equals(status);
	}
	
}
