package io.pismo.techchallenge.produtos.constants;

public enum StatusReserva {
	
	SOLICITADA,
	CONFIRMADA,
	CANCELADA;

}
