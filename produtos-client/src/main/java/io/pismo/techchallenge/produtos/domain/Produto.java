package io.pismo.techchallenge.produtos.domain;

import java.util.UUID;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table
public class Produto {
	
	@PrimaryKey
	private String id;
	
	private String nome;
	private Integer quantidade;
	
	public Produto() {
		this(null, null);
	}
	
	public Produto(String nome, Integer quantidade) {
		this.id = UUID.randomUUID().toString();
		this.nome = nome;
		this.quantidade = quantidade;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public boolean realizaReserve(Reserva reserva) {
		if (reserva.getQuantidade() > quantidade) {
			return false;
		} else {
			quantidade -= reserva.getQuantidade();
			return true;
		}
	}
	
	@Override
	public String toString() {
		return "Produto [id=" + id + ", nome=" + nome + ", quantidade=" + quantidade + "]";
	}

}
