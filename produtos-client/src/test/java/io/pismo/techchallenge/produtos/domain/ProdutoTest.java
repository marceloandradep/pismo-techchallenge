package io.pismo.techchallenge.produtos.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ProdutoTest {
	
	@Test
	public void quando_quantidade_reservada_maior_que_a_quantidade_produtos_nao_deve_reservar() {
		Produto produto = new Produto("P1", 5);
		Reserva reserva = new Reserva(null, null, 7);
		
		boolean reservou = produto.realizaReserve(reserva);
		
		assertFalse(reservou);
	}
	
	@Test
	public void quando_quantidade_reservada_igual_a_quantidade_de_produtos_deve_reservar() {
		Produto produto = new Produto("P1", 5);
		Reserva reserva = new Reserva(null, null, 5);
		
		boolean reservou = produto.realizaReserve(reserva);
		
		assertTrue(reservou);
	}
	
	@Test
	public void quando_quantidade_reservada_menor_a_quantidade_de_produtos_deve_reservar() {
		Produto produto = new Produto("P1", 5);
		Reserva reserva = new Reserva(null, null, 3);
		
		boolean reservou = produto.realizaReserve(reserva);
		
		assertTrue(reservou);
	}

}
