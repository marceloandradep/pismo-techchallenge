package io.pismo.techchallenge.compras.services;

import javax.jms.JMSException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConfirmacaoReservaListenerTest {
	
	@Autowired
	private ConfirmacaoReservaListener listener;
	
	@MockBean(name = "pedidoService")
	private PedidoService pedidoService;
	
	@Test
	public void quando_confirmar_reserva_finalizar_pedido() throws JMSException {
		String reservaJson = "{\"status\" : \"CONFIRMADA\", \"pedidoId\" : 1}";
		
		listener.respostaReserva(reservaJson);
		
		BDDMockito.verify(pedidoService).confirmaPedido(1);
	}
	
	@Test
	public void quando_cancelar_reserva_cancelar_pedido() throws JMSException {
		String reservaJson = "{\"status\" : \"CANCELADA\", \"pedidoId\" : 1}";
		
		listener.respostaReserva(reservaJson);
		
		BDDMockito.verify(pedidoService).cancelaPedido(1);
	}

}
