package io.pismo.techchallenge.compras.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.pismo.techchallenge.compras.domain.Pedido;
import io.pismo.techchallenge.compras.repository.PedidoRepository;
import io.pismo.techchallenge.compras.services.PedidoService;
import io.pismo.techchallenge.compras.services.ProdutoService;

@RunWith(SpringRunner.class)
@WebMvcTest(PedidoController.class)
public class PedidoControllerTest {
	
	@MockBean
	private PedidoService pedidoService;
	
	@MockBean
	private PedidoRepository pedidoRepository;
	
	@MockBean
	private ProdutoService produtoService;
	
	@MockBean
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void submeter_pedido() throws JsonProcessingException, Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		Pedido pedido = new Pedido("abc", 5);
		
		given(pedidoService.submeterPedido(any())).willReturn(pedido);
		
		MvcResult result = mvc.perform(
				post("/v1/pedidos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(pedido))
				).andExpect(status().isOk()).andReturn();
		
		Pedido pedidoRetornado = objectMapper.readValue(result.getResponse().getContentAsString(), Pedido.class);
		
		assertThat(pedidoRetornado.getProdutoId()).isEqualTo(pedido.getProdutoId());
		assertThat(pedidoRetornado.getQuantidade()).isEqualTo(pedido.getQuantidade());
	}

}
