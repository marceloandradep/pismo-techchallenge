package io.pismo.techchallenge.compras.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.pismo.techchallenge.compras.domain.Pedido;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PedidoRepositoryTest {
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Test
	public void test() {
		Pedido pedido = new Pedido("1", 5);
		
		pedido = pedidoRepository.save(pedido);
		pedido = pedidoRepository.findOne(pedido.getId());
		
		assertThat(pedido).isNotNull();
		assertThat(pedido.getProdutoId()).isEqualTo("1");
		assertThat(pedido.getQuantidade()).isEqualTo(5);
	}

}
