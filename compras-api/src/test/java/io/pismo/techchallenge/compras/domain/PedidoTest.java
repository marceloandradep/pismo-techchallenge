package io.pismo.techchallenge.compras.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@JsonTest
public class PedidoTest {
	
	@Autowired
	private JacksonTester<Pedido> json;
	
	@Test
	public void json_serialization() throws IOException {
		Pedido pedido = new Pedido("abc", 5);
		assertThat(json.write(pedido)).isEqualToJson("pedido.json");
	}
	
}
