package io.pismo.techchallenge.compras.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import io.pismo.techchallenge.compras.domain.Pedido;
import io.pismo.techchallenge.compras.domain.StatusPedido;
import io.pismo.techchallenge.compras.repository.PedidoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PedidoServiceTest {
	
	@Autowired
	private PedidoService service;
	
	@MockBean(name = "pedidoRepository")
	private PedidoRepository pedidoRepository;
	
	@MockBean(name = "produtoService")
	private ProdutoService produtoService;
	
	@Test
	public void quando_submeter_pedido_salvar_pedido_e_reservar_inventario() {
		Pedido pedido = new Pedido("abc", 5);
		
		given(pedidoRepository.save(pedido)).willReturn(pedido);
		
		service.submeterPedido(pedido);
		
		BDDMockito.verify(pedidoRepository).save(pedido);
		BDDMockito.verify(produtoService).reservar(pedido);
	}
	
	@Test
	public void quando_confirmar_alterar_status_para_finalizado_e_salvar() {
		Pedido pedido = new Pedido("abc", 5);
		
		given(pedidoRepository.findOne(1)).willReturn(pedido);
		
		service.confirmaPedido(1);
		
		assertThat(pedido.getStatus()).isEqualTo(StatusPedido.FINALIZADO);
		BDDMockito.verify(pedidoRepository).save(pedido);
	}
	
	@Test
	public void quando_cancelar_alterar_status_para_cancelar_e_salvar() {
		Pedido pedido = new Pedido("abc", 5);
		
		given(pedidoRepository.findOne(1)).willReturn(pedido);
		
		service.cancelaPedido(1);
		
		assertThat(pedido.getStatus()).isEqualTo(StatusPedido.CANCELADO);
		BDDMockito.verify(pedidoRepository).save(pedido);
	}

}
