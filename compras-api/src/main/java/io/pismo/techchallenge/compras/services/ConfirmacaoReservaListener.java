package io.pismo.techchallenge.compras.services;

import java.io.IOException;

import javax.jms.JMSException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import io.pismo.techchallenge.produtos.domain.Reserva;

@Service
public class ConfirmacaoReservaListener {

	@Autowired
	private PedidoService pedidoService;
	
	@JmsListener(destination = "confirmacao")
	public void respostaReserva(String reservaJson) throws JMSException {
		try {
			Reserva reserva = Reserva.fromJson(reservaJson);
			
			if (reserva.isConfirmada()) {
				pedidoService.confirmaPedido(reserva.getPedidoId());
			} else {
				pedidoService.cancelaPedido(reserva.getPedidoId());
			}
		} catch (IOException e) {
			throw new RuntimeException("Error while parsing json.", e);
		}
	}

}
