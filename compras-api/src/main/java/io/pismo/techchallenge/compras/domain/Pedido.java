package io.pismo.techchallenge.compras.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pedido {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String produtoId;
	private Integer quantidade;
	
	private StatusPedido status;
	
	public Pedido() {
		this(null, null);
	}
	
	public Pedido(String produtoId, Integer quantidade) {
		this.produtoId = produtoId;
		this.quantidade = quantidade;
		this.status = StatusPedido.PROCESSANDO;
	}
	
	public Integer getId() {
		return id;
	}

	public String getProdutoId() {
		return produtoId;
	}

	public void setProdutoId(String produtoId) {
		this.produtoId = produtoId;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public StatusPedido getStatus() {
		return status;
	}
	
	public void setStatus(StatusPedido status) {
		this.status = status;
	}
	
	public void confirma() {
		status = StatusPedido.FINALIZADO;
	}
	
	public void cancela() {
		status = StatusPedido.CANCELADO;
	}
	
	@Override
	public String toString() {
		return "Pedido: [produto=" + produtoId + ", quantidade=" + quantidade + "]";
	}

}
