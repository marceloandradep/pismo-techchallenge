package io.pismo.techchallenge.compras.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.pismo.techchallenge.compras.domain.Pedido;
import io.pismo.techchallenge.produtos.domain.Reserva;

@Service
public class ProdutoService {

	@Autowired
	private JmsTemplate defaultJmsTemplate;
	
	@Value("${queue.reservas}")
	private String queue;
	
	public void reservar(Pedido pedido) {
		Reserva reserva = new Reserva(pedido.getProdutoId(), pedido.getId(), pedido.getQuantidade());
		try {
			defaultJmsTemplate.convertAndSend(queue, reserva.toJson());
		} catch (JmsException | JsonProcessingException e) {
			throw new RuntimeException("Error while serializing object to json.", e);
		}
	}

}
