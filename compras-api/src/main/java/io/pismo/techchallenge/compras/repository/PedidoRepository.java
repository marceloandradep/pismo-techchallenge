package io.pismo.techchallenge.compras.repository;

import org.springframework.data.repository.CrudRepository;

import io.pismo.techchallenge.compras.domain.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Integer> {
}
