package io.pismo.techchallenge.compras.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.pismo.techchallenge.compras.domain.Pedido;
import io.pismo.techchallenge.compras.repository.PedidoRepository;

@Service
public class PedidoService {
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private ProdutoService produtoService;
	
	public Pedido submeterPedido(Pedido pedido) {
		pedido = pedidoRepository.save(pedido);
		
		produtoService.reservar(pedido);
		
		return pedido;
	}
	
	public Pedido obterPedido(Integer id) {
		return pedidoRepository.findOne(id);
	}

	public void confirmaPedido(Integer pedidoId) {
		Pedido pedido = pedidoRepository.findOne(pedidoId);
		pedido.confirma();
		pedidoRepository.save(pedido);
	}
	
	public void cancelaPedido(Integer pedidoId) {
		Pedido pedido = pedidoRepository.findOne(pedidoId);
		pedido.cancela();
		pedidoRepository.save(pedido);
	}

	public Iterable<Pedido> obterTodosPedidos() {
		return pedidoRepository.findAll();
	}

}
