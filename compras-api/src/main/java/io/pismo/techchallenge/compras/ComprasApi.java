package io.pismo.techchallenge.compras;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComprasApi {
	
	public static void main(String[] args) {
		SpringApplication.run(ComprasApi.class, args);
	}

}
