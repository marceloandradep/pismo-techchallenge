package io.pismo.techchallenge.compras.domain;

public enum StatusPedido {
	
	PROCESSANDO,
	FINALIZADO,
	CANCELADO;

}
