package io.pismo.techchallenge.compras.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.pismo.techchallenge.compras.domain.Pedido;
import io.pismo.techchallenge.compras.services.PedidoService;

@RestController
@RequestMapping("/v1")
public class PedidoController {
	
	@Autowired
	private PedidoService pedidoService;
	
	@RequestMapping(path = "/pedidos", method = RequestMethod.POST)
	public Pedido submeter(@RequestBody Pedido pedido) {
		return pedidoService.submeterPedido(pedido);
	}
	
	@RequestMapping(path = "/pedidos", method = RequestMethod.GET)
	public Iterable<Pedido> obterTodosPedidos() {
		return pedidoService.obterTodosPedidos();
	}
	
	@RequestMapping(path = "/pedidos/{id}", method = RequestMethod.GET)
	public Pedido obterPedido(@PathVariable Integer id) {
		return pedidoService.obterPedido(id);
	}

}
