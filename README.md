# Integração de APIs para venda de produtos

Foi desenvolvido duas APIs: compras-api e produtos-api. A produtos-api disponibiliza operações de CRUD para produtos. A compras-api fornece endpoints para a realização de compras a partir dos produtos disponibilizados pela produtos-api. Um fluxo de um venda seria:
1. Consultar um produto através da produtos-api e guardar o seu id;
```
GET http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3001/v1/produtos
```
2. Montar um json com o pedido contento a quantidade e o id do produto desejado. A resposta desta requisição é o id de uma reserva;
```
POST http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3000/v1/pedidos

{
  "produtoId" : "aaf7259e-56ed-459a-a137-2bccfb91bad0",
  "quantidade" : 1
}
```
3. Como implementei o processo de maneira assíncrona, o id deve ser utilizado para consultar a situação do pedido.
```
Exemplo:
GET http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3000/v1/pedidos/3
```

# Considerações sobre a estratégia adotada

Como a API de produtos e a de compras recebem demandas diferentes, optei por construir cada uma como um micro-serviço separado utilizando Spring Boot. Pelo suposição de que os clientes vão pesquisar produtos muito mais do que comprar, julguei importante que a API de produtos utilizasse um banco distribuído como o Cassandra. Assim, dependendo da demanda posso escalar horizontalmente colocando mais nós. Para a API de compras optei por um banco MySQL pelo suporte a transações, necessário na realização das compras.

Utilizei uma comunicação baseada em JMS entre os micro-serviços para que caso a API de produtos estivesse fora não inviabilizasse o processo de compra. Além disso, dependendo da quantidade de usuários comprando é possível instanciar mais instâncias de produtos-api para consumir os pedidos.

Dois casos que não tratei explicitamente: o banco estar fora e o caso de uma demora na resposta da produtos-api.

Para o caso do banco eu poderia usar um cache de dados como o redis, ou então colocar a produtos-api atrás de um API gateway. Acredito que estes são serviços que o AWS fornece, e o Spring Cloud Netflix OSS provê implementações de API gateway, bem como de service discovery que tratam a indisponibilidade temporária dos serviços.

No caso da demora, eu poderia usar uma framework chamado Hystrix que é um Circuit Breaker. Ele basicamente implementa um thread pool próprio que administra todas as requisições. Caso ele detecte uma demora, e perceba que as threads estão se acumulando, ele começa a retornar falha, o que por sua vez aciona o service discovery para substituir a instância do serviço ou o elastic load balancer para que ele instancie mais instâncias.

Não estou muito certo sobre esses cenários, pois tenho pouca experiência em projetos reais com este tipo de abordagem.

## Instalação

Baixe o repositório.
```
git clone https://marceloandradep@bitbucket.org/marceloandradep/pismo-techchallenge.git
```
Acesse o diretório do repositório.
```
cd pismo-techchallenge
```
Construa o pacote da aplicação.
```
mvn package
```
Execute a aplicação.
```
java -jar compras-api\target\compras-api.jar
java -jar produtos-api\target\produtos-api.jar
```
É possível também acessar a aplicação na AWS.
```
compras-api http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3000
produtos-api http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3001
```

## API de produtos
Você pode cadastrar novos produtos utilizando:
```
POST http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3001/v1/produtos
{
  "nome":"Nintendo Switch",
  "quantidade": 43
}
```
A API retornará no response o mesmo json com a adição do id cadastrado para o produto.

Para consultar todos os produtos utilize:
```
GET http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3001/v1/produtos
```

Para consultar um produto por id utilize:
```
GET http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3001/v1/produtos/{id}
```
onde o {id} é o id do produto que deseja consultar.

## API de compras
Para realizar uma compra utilize:
```
POST http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3000/v1/pedidos
{
  "produtoId" : {idProduto},
  "quantidade" : {quantidade}
}
```
onde {idProduto} é o id do produto que deseja comprar e {quantidade} a quantidade do pedido. A API irá retornar um json contento o pedido com o id associado. Exemplo:
```
{
  "id": 1,
  "produtoId": "abc",
  "quantidade": 5,
  "status": "PROCESSANDO"
}
```

Para consultar o status do pedido utilize:
```
GET http://ec2-35-165-91-108.us-west-2.compute.amazonaws.com:3000/v1/pedidos/{id}
```
onde {id} é o id do pedido o qual se deseja saber o status.

## Executando os testes unitários
Para rodar os testes unitários utilize:
```
mvn test
```