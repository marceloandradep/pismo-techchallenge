package io.pismo.techchallenge.produtos.repository;

import org.springframework.data.repository.CrudRepository;

import io.pismo.techchallenge.produtos.domain.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, String> {
}
