package io.pismo.techchallenge.produtos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.pismo.techchallenge.produtos.domain.Produto;
import io.pismo.techchallenge.produtos.domain.Reserva;
import io.pismo.techchallenge.produtos.repository.ProdutoRepository;

@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private JmsTemplate defaultJmsTemplate;
	
	@Value("${queue.reply}")
	private String queue;
	
	public Produto salvarProduto(Produto produto) {
		return produtoRepository.save(produto);
	}
	
	public Produto obterProduto(String id) {
		return produtoRepository.findOne(id);
	}

	public void realizaReserva(Reserva reserva) {
		Produto produto = produtoRepository.findOne(reserva.getProdutoId());
		
		try {
			if (produto.realizaReserve(reserva)) {
				produtoRepository.save(produto);
				defaultJmsTemplate.convertAndSend(queue, reserva.confirma().toJson());
			} else {
				defaultJmsTemplate.convertAndSend(queue, reserva.cancela().toJson());
			}
		} catch (JmsException | JsonProcessingException e) {
			throw new RuntimeException("Error while serializing object to json.", e);
		}
	}

	public Iterable<Produto> obterTodosProdutos() {
		return produtoRepository.findAll();
	}

}
