package io.pismo.techchallenge.produtos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.pismo.techchallenge.produtos.domain.Produto;
import io.pismo.techchallenge.produtos.services.ProdutoService;

@RestController
@RequestMapping("/v1")
public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;
	
	@RequestMapping(path = "/produtos", method = RequestMethod.POST)
	public @ResponseBody Produto salvarProduto(@RequestBody Produto produto) {
		return produtoService.salvarProduto(produto);
	}
	
	@RequestMapping(path = "/produtos/{id}", method = RequestMethod.GET)
	public Produto obterProduto(@PathVariable String id) {
		return produtoService.obterProduto(id);
	}
	
	@RequestMapping(path = "/produtos", method = RequestMethod.GET)
	public Iterable<Produto> obterTodosProduto() {
		return produtoService.obterTodosProdutos();
	}

}
