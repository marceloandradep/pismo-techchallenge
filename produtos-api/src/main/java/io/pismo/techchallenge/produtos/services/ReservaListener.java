package io.pismo.techchallenge.produtos.services;

import java.io.IOException;

import javax.jms.JMSException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import io.pismo.techchallenge.produtos.domain.Reserva;

@Service
public class ReservaListener {
	
	@Autowired
	private ProdutoService produtoService;
	
	@JmsListener(destination = "reservas")
	public void realizaReserve(String reservaJson) throws JMSException {
		try {
			Reserva reserva = Reserva.fromJson(reservaJson);
			produtoService.realizaReserva(reserva);
		} catch (IOException e) {
			throw new RuntimeException("Error while parsing json: " + reservaJson, e);
		}
	}

}
